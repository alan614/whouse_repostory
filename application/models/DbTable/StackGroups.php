<?php
class Model_DbTable_StackGroups extends Zend_Db_Table {
	protected $_name	= 'stack_groups';
	protected $_primary	= 'id';
	
	protected $_dependentTables	= array(
		'Model_DbTable_Stacks'
	);
}