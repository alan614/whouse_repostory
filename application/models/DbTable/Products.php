<?php
class Model_DbTable_Products extends Zend_Db_Table {
    protected $_name    = 'products';
    protected $_primary = 'id';
    
    protected $_referenceMap    = array(
        'Stack' => array(
            'columns'   => 'stack_id',
            'refTableClass' => 'Model_DbTable_Stacks',
            'refColumns'    => 'id'
        )
    );
}