<?php
class Model_DbTable_Stacks extends Zend_Db_Table {
    protected $_name    = 'stacks';
    protected $_primary = 'id';
    
    protected $_dependentTables = array(
        'Model_DbTable_Products'
    );
    
    protected $_referenceMap	= array(
    	'StackGroup'	=> array(
    		'columns'	=> array('stack_group_id'),
    		'refTableClass'	=> 'Model_DbTable_StackGroups',
    		'refColumns'	=> array('id')
    	)
    );
}