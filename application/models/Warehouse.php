<?php
class Model_Warehouse {
	protected $_stacks	= null;
	protected $_resultSetLimit	= 10;
	
	protected $_dbProducts = null, $_dbStacks = null, $dbStackGroups = null;
	
	public function __construct(Model_DbTable_Products $products, Model_DbTable_Stacks $stacks, Model_DbTable_StackGroups $stackGroups) {
		$this->_dbProducts	= $products;
		$this->_dbStacks	= $stacks;
		$this->_dbStackGroups	= $stackGroups;
	}
	
	public function getDbProducts() {
		return $this->_dbProducts;
	}
	
	public function getDbStacks() {
		return $this->_dbStacks;
	}
	
	public function getDbStackGroups() {
		return $this->_dbStackGroups;
	}
	
	public function setResultSetLimit($limit = 10) {
		$limit	= intval($limit);
		$this->_resultSetLimit	= $limit == 0 ? 10 : $limit;
		return $this;
	}
	
	public function getResultSetLimit() {
		return $this->_resultSetLimit;
	}
	
	public function getProducts($nameFilter = '', $page = 1) {
		$resultSet	= new Model_Warehouse_ResultSet_Product();
		$query	= $this->_dbProducts->select()->from('products', array('id'));
		if(trim($nameFilter) != '') $query->where('name LIKE ?', '%'.$nameFilter.'%');
		$paginator	= new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($query));
		$paginator->setItemCountPerPage($this->getResultSetLimit());
		$paginator->setCurrentPageNumber($page);
		$resultSet->generateFromPaginator($paginator, $this->_dbProducts);
		return $resultSet;
	}
	
	public function getProductById($id) {
		$row	= $this->getDbProducts()->find($id)->current();
		if($row) return new Model_Warehouse_Product($row);
		else return null;
	}
	
	public function getStackByName($chars) {
		$row	= $this->getDbStacks()->fetchRow(array('name LIKE ?' => $chars));
		if($row) return new Model_Warehouse_Stack($row, $this->getDbProducts());
		else return null;
	}
	
	public function createOrder() {
		return new Model_Warehouse_Order($this->getDbProducts(), $this->getDbStacks(), $this->getDbStackGroups());
	}
}