<?php
class Model_Warehouse_Stack extends Model_Warehouse_Abstract {
	protected $_dbRow	= null;
	
	protected $_dbTableProducts	= null;
	
	protected $_id = null, $_stack_group_id = null, $_name = null, $_order = null;
	
	protected $_resultSetLimit	= 10;
	
	public function __construct(Zend_Db_Table_Row_Abstract $stackRow, Model_DbTable_Products $dbProducts) {
		if($stackRow->getTableClass() != 'Model_DbTable_Stacks') throw new InvalidArgumentException('Supplied row is not a product stack');
		$this->_dbTableProducts	= $dbProducts;
		$this->_dbRow	= $stackRow;
		$this->_bindRowToClass();
	}
	
	public function getDbRow() {
		return $this->_dbRow;
	}
	
	public function getDbTableProducts() {
		return $this->_dbTableProducts;
	}
	
	protected function _bindRowToClass() {
		$thisRow	= $this->getDbRow();
		$this->_id	= $thisRow['id'];
		$this->_stack_group_id	= $thisRow['stack_group_id'];
		$this->_name	= $thisRow['name'];
		$this->_order	= $thisRow['order'];
		return $this;
	}
	
	public function setResultSetLimit($limit = 10) {
		$limit	= intval($limit);
		$this->_resultSetLimit	= $limit == 0 ? 10 : $limit;
		return $this;
	}
	
	public function getResultSetLimit() {
		return $this->_resultSetLimit;
	}
	
	public function getId() {
		return $this->_id;
	}
	
	public function getStackGroupId() {
		return $this->_stack_group_id;
	}
	
	public function getName() {
		return $this->_name;
	}
	
	public function setName($string) {
		$this->_name	= htmlentities(strip_tags(strval($string)));
		return $this;
	}
	
	public function getOrder() {
		return $this->_order;
	}
	
	public function setOrder($order) {
		$this->_order	= intval($order);
		return $this;
	}
	
	public function getProducts($pageNum = 1) {
		$query	= $this	->getDbTableProducts()->select()
						->from(array('p' => 'products'))
						->where('stack_id = ?', $this->getId());
		$paginator	= new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($query));
		$paginator->setItemCountPerPage($this->getResultSetLimit());
		$paginator->setCurrentPageNumber($pageNum);
		
		$resultSet	= new Model_Warehouse_ResultSet_Product();
		$resultSet->generateFromPaginator($paginator, $this->getDbTableProducts());
		return $resultSet;
	}
	
	public function getProductByBinNum($binNum = 1) {
		$query	= $this->getDbRow()->select()->where('bin_num = ?', $binNum);
		$product	= $this->getDbRow()->findModel_DbTable_Products($query)->current();
		if($product) return new Model_Warehouse_Product($product);
		else return null;
	}
	
	public function toArray() {
		return array(
			'id'	=> $this->getId(),
			'stack_group_id'	=> $this->getStackGroupId(),
			'name'	=> $this->getName(),
			'order'	=> $this->getOrder()
		);
	}
	
	public function save() {
		$thisRow	= $this->getDbRow();
		$thisRow['name']	= $this->getName();
		//$thisRow['stack_group_id']	= $this->getStackGroupId();
		$thisRow['order']	= $this->getOrder();
		return $this->_bindRowToClass();
	}
}