<?php
abstract class Model_Warehouse_ResultSet_Abstract extends SplDoublyLinkedList implements JsonSerializable {
	
	protected $_paginatorItemPerPage	= 0;
	protected $_paginatorTotalItemCount	= 0;
	protected $_paginatorCurrentPage	= 1;
	
	public function toArray() {
		$output	= array();
		foreach($this as $entry) $output[]	= $entry->toArray();
		return $output;
	}
	
	public function jsonSerialize() {
		return $this->toArray();
	}
	
	public function copyPaginatorProps(Zend_Paginator $paginator) {
		$this->_paginatorItemPerPage	= $paginator->getItemCountPerPage();
		$this->_paginatorTotalItemCount	= $paginator->getTotalItemCount();
		$this->_paginatorCurrentPage	= $paginator->getCurrentPageNumber();
		return $this;
	}
	
	public function getItemPerPage() {
		return $this->_paginatorItemPerPage;
	}
	
	public function getTotalItemCount() {
		return $this->_paginatorTotalItemCount;
	}
	
	public function getCurrentPage() {
		return $this->_paginatorCurrentPage;
	}
	
	abstract public function generateFromPaginator(Zend_Paginator $paginator, Zend_Db_Table_Abstract $sourceTable);
}