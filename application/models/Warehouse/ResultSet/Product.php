<?php
class Model_Warehouse_ResultSet_Product extends Model_Warehouse_ResultSet_Abstract {
	public function generateFromPaginator(Zend_Paginator $paginator, Zend_Db_Table_Abstract $sourceTable) {
		if(!($sourceTable instanceof Model_DbTable_Products)) throw new InvalidArgumentException('Supplied table is not the products table');
		$this->copyPaginatorProps($paginator);
		foreach($paginator as $rowWithId) {
			$thisRow	= $sourceTable->find($rowWithId['id'])->current();
			if($thisRow) $this->push(new Model_Warehouse_Product($thisRow));
		}
		return $this;
	}
}