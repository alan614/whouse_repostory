<?php
Class Model_Warehouse_Order extends Model_Warehouse_Abstract {
	
	protected $_dbProducts	= null, $_dbStacks	= null, $_dbStackGroups	= null;
	protected $_products	= null;
	
	protected $_stacks	= array();
	protected $_stackGroups	= array();
	protected $_stackSequence	= array();
	protected $_picker	= null;
	
	public function __construct(Model_DbTable_Products $dbProducts, Model_DbTable_Stacks $dbStacks, Model_DbTable_StackGroups $dbStackGroups) {
		$this->_dbProducts	= $dbProducts;
		$this->_dbStacks	= $dbStacks;
		$this->_dbStackGroups	= $dbStackGroups;
		
		$this->_products	= new SplObjectStorage();
		$this->_picker	= new Model_Warehouse_Order_Picker();
	}
	
	public function getDbProducts() {
		return $this->_dbProducts;
	}
	
	public function getDbStacks() {
		return $this->_dbStacks;
	}
	
	public function getDbStackGroups() {
		return $this->_dbStackGroups;
	}
	
	public function addProductById($productId, $orderNum) {
		$thisRow	= $this->getDbProducts()->find($productId)->current();
		if($thisRow) {
			$this->addProductByRow($thisRow, $orderNum);
		}
		return $this;
	}
	
	public function addProductByRow(Zend_Db_Table_Row_Abstract $row, $orderNum) {
		if($row->getTableClass() != 'Model_DbTable_Products') throw new InvalidArgumentException('Supplied row is not a product');
		$thisProductModel	= new Model_Warehouse_Order_Product($row, $orderNum);
		$this->_products->attach($thisProductModel);
		return $this;
	}
	
	public function getProducts() {
		return $this->_products;
	}
	
	protected function _buildOrderStacks() {
		$productIds	= array();
		foreach($this->getProducts() as $product) $productIds[]	= $product->getId();
		$select	= $this	->getDbStackGroups()->select()->distinct()
						->from(array('sg' => 'stack_groups'), array('id'))
						->joinInner(array('s' => 'stacks'), 'sg.id=s.stack_group_id', null)
						->joinInner(array('p' => 'products'), 's.id=p.stack_id', null)
						->where('p.id IN (?)', $productIds)
						->order('sg.order ASC');
		$stackGroupIds	= $this->getDbStackGroups()->fetchAll($select);
		foreach($stackGroupIds as $stackGroupId) {
			$thisRow	= $this->getDbStackGroups()->find($stackGroupId['id'])->current();
			if($thisRow) $this->_stackGroups[$thisRow['id']]	= new Model_Warehouse_Order_StackGroup($thisRow);
		}
		return $this;
	}
	
	public function &getOrderStackGroups() {
		if(empty($this->_stackGroups)) $this->_buildOrderStacks();
		return $this->_stackGroups;
	}
	
	public function evaluateOrderAvailability() {
		if(count($this->getProducts()) > 0) {
			$report	= array(
				'available'	=> true,
				'badProducts'	=> array(),
				'message'	=> 	'',
				'code'	=> 0
			);
			foreach($this->getProducts() as $product) {
				if(!$product->hasEnoughStock()) {
					$report['available']	= false;
					$report['badProducts'][]	= $product;
					$report['code']	= 1;
				}
			}
			return $report;
		}
		else {
			return null;
		}
	}
	
	protected function _sortProductsToStackGroups() {
		foreach($this->getProducts() as $product) {
			$stackGroupId	= $product->getStackGroupId();
			if(isset($this->_stackGroups[$stackGroupId])) $this->_stackGroups[$stackGroupId]->addProduct($product);
		}
	}
	
	public function getPicker() {
		return $this->_picker;
	}
	
	public function findRoute() {
		$stackGroups	= $this->getOrderStackGroups();
		$this->_sortProductsToStackGroups();
		$picker	= $this->getPicker();
		$lastStack	= end(array_values($stackGroups));
		$picker->setStackGroups(array_values($stackGroups));
		$picker->processStackGroups();
		return $picker->getRoute();
	}
	
	public function clear() {
		
	}
	
	public function toArray() {
		return array();
	}
}