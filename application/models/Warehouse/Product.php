<?php
class Model_Warehouse_Product extends Model_Warehouse_Abstract {
	protected $_dbRow	= null;
	protected $_dbRowStack	= null;
	
	protected $_id = null, $_name = null, $_stackId = null, $_binNum = null, $_stockNum = null, $_description = null;
	
	public function __construct(Zend_Db_Table_Row $row) {
		if($row->getTableClass() != 'Model_DbTable_Products') throw new InvalidArgumentException('Supplied row is not a product.');
		$this->_dbRow	= $row;
		$this->_bindRowToClass();
	}
	
	protected function _bindRowToClass() {
		$row	= $this->_dbRow;
		$this->_id	= $row['id'];
		$this->_name	= $row['name'];
		$this->_description	= $row['description'];
		$this->_stackId	= $row['stack_id'];
		$this->_binNum	= $row['bin_num'];
		$this->_stockNum	= $row['stock_num'];
		return $this;
	}
	
	public function getDbRow() {
		return $this->_dbRow;
	}
	
	public function getStackRow() {
		if($this->_dbRowStack == null) {
			$thisRowStack	= $this->_dbRow->findParentModel_DbTable_Stacks();
			if(!$thisRowStack) throw new LogicException('Product does not belong to any stack');
			$this->_dbRowStack	= $thisRowStack;
		}
		return $this->_dbRowStack;
	}
	
	public function getId() {
		return $this->_id;
	}
	
	public function getName() {
		return $this->_name;
	}
	
	public function setName($name) {
		$this->_name	= htmlentities(strip_tags(strval($name)));
		return $this;
	}
	
	public function getDescription() {
		return $this->_description;
	}
	
	public function setDescription($description) {
		$this->_description	= htmlentities(strip_tags($description));
		return $this;
	}
	
	public function getStackId() {
		return $this->_stackId;
	}
	/*
	public function setStackId() {
		
	}
	*/
	public function getBinNum() {
		return $this->_binNum;
	}
	/*
	public function setBinNum() {
		
	}
	*/
	
	public function getBin() {
		$stackRow	= $this->getStackRow();
		return $stackRow['name'] . $this->getBinNum();
	}
	
	public function setBin() {
		//placeholder function to move products to another bit
	}
	
	public function getStockNum() {
		return $this->_stockNum;
	}
	
	public function setStockNum($stock) {
		$this->_stockNum	= intval($stock);
		return $this;
	}
	
	public function stockDecrement($amount = 1) {
		if($this->isStockEnough($amount)) {
			$this->_stockNum	-= $amount;
			return $this;
		}
		else throw new OutOfRangeException('Not enough stock to fulfill command');
	}
	
	public function stockIncrement($amount = 1) {
		$this->_stockNum	+= $amount;
		return $this;
	}
	
	public function isStockEnough($amountToRemove = 0) {
		if(($this->getStockNum() - intval($amountToRemove) ) >= 0) return true;
		return false;
	}
	
	public function toArray() {
		return array(
			'id'	=> $this->getId(),
			'name'	=> $this->getName(),
			'description'	=> $this->getDescription(),
			'stack_id'	=> $this->getStackId(),
			'bin_num'	=> $this->getBinNum(),
			'stock_num'	=> $this->getStockNum(),
			'bin'	=> $this->getBin()
		);
	}
	
	public function save() {
		$thisRow	= $this->getDbRow();
		$thisRow['name']	= $this->getName();
		$thisRow['description']	= $this->getDescription();
		$thisRow['stock_num']	= $this->getStockNum();
		/* //may be enabled later on to allow relocation of products
		$thisRow['stack_id']	= '';
		$thisRow['bin_num']	= '';
		*/
		$thisRow->save();
		return $this->_bindRowToClass();
	}
}