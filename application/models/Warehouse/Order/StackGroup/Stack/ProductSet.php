<?php
class Model_Warehouse_Order_StackGroup_Stack_ProductSet implements Iterator, Countable, ArrayAccess, JsonSerializable {
	protected $_data	= array();
	
	private $_count	= null;
	private $_position	= null;
	
	public function __construct() {
		$this->_position	= 0;
		$this->_count	= 0;
	}
	
	public function current() {
		return $this->_data[$this->key()];
	}
	
	public function key() {
		return $this->_position;
	}
	
	public function next() {
		++$this->_position;
		return $this;
	}
	
	public function prev() {
		--$this->_position;
		return $this;
	}
	
	public function rewind() {
		$this->_position	= 0;
		return $this;
	}
	
	public function valid() {
		return isset($this->_data[$this->key()]);
	}
	
	public function push(Model_Warehouse_Order_Product $product) {
		$this->_data[]	= $product;
		usort($this->_data, array($this, 'sort'));
		$this->_count++;
		return $this;
	}
	
	public function remove($key) {
		if(isset($this->_data[$key])) {
			unset($this->_data[$key]);
			usort($this->_data, array($this, 'sort'));
			$this->_count--;
		}
		return $this;
	}
	
	private function sort($var1, $var2) {
		return $var1->getBinNum() - $var2->getBinNum();
	}
	
	private function revSort($var1, $var2) {
		return $var2->getBinNum() - $var1->getBinNum();
	}
	
	public function sortFromBottom() {
		usort($this->_data, array($this, 'sort'));
		$this->rewind();
		return $this;
	}
	
	public function sortFromTop() {
		usort($this->_data, array($this, 'revSort'));
		$this->rewind();
		return $this;
	}
	
	public function count() {
		return $this->_count;
	}
	
	public function offsetExists($offset) {
		return $this->valid($offset);
	}
	
	public function offsetGet($offset) {
		if(!isset($this->_data[$offset])) throw new OutOfBoundsException('Element is not in the ResultSet');
		else return $this->_data[$offset];
	}
	
	public function offsetSet($offset, $value) {
		throw new LogicException('Use push method instead to insert a new element');
	}
	
	public function offsetUnset($offset) {
		return $this->remove($offset);
	}
	
	public function toArray() {
		$output	= array();
		foreach($this as $item) $output[]	= $item->toArray();
		return $output;
	}
	
	public function jsonSerialize() {
		return $this->toArray();
	}
}