<?php
class Model_Warehouse_Order_StackGroup_Stack extends Model_Warehouse_Stack {
	
	protected $_products	= null;
	
	public function __construct(Zend_Db_Table_Row_Abstract $stackRow) {
		if($stackRow->getTableClass() != 'Model_DbTable_Stacks') throw new InvalidArgumentException('Supplied row is not a product stack');
		$this->_dbRow	= $stackRow;
		$this->_bindRowToClass();
		$this->_products	= new Model_Warehouse_Order_StackGroup_Stack_ProductSet();
	}
	
	public function addProduct(Model_Warehouse_Order_Product $thisProduct) {
		if($thisProduct->getStackId() == $this->getId()) $this->_products->push($thisProduct);
		return $this;
	}
	
	public function getProducts($startFrom = 'bottom') {
		//Zend_Debug::dump($this->_products->toArray());
		//return $this->_products;
		if($startFrom == 'bottom') return $this->_products->sortFromBottom();
		else {
			return $this->_products->sortFromTop();
		}
	}
	
	public function hasProducts() {
		return count($this->_products) > 0;
	}
	
	public function getMaxBins() {
		$select	= $this	->getDbRow()->select()->setIntegrityCheck(false)
						->from('products', array('maxBin' => 'MAX(bin_num)'))
						->where('stack_id = ?', $this->getId());
		$thisResult	= $this->getDbRow()->getTable()->fetchRow($select);
		return $thisResult['maxBin'];
	}
}