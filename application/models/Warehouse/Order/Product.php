<?php
class Model_Warehouse_Order_Product extends Model_Warehouse_Product {
	
	protected $_orderAmount	= 0;
	
	public function __construct(Zend_Db_Table_Row $row, $orderCount = null) {
		parent::__construct($row);
		if(intval($orderCount) > 0) $this->setOrderAmount($orderCount);
	}
	
	public function getOrderAmount() {
		return $this->_orderAmount;
	}
	
	public function setOrderAmount($amount) {
		$this->_orderAmount	= intval($amount);
		return $this;
	}
	
	public function incrementOrder() {
		$this->_orderAmount++;
		return $this;
	}
	
	public function decrementOrder() {
		$this->_orderAmount--;
		return $this;
	}
	
	public function hasEnoughStock() {
		return $this->getStockNum() >= $this->getOrderAmount();
	}
	
	public function getStackGroupId() {
		$stackGroupRow	= $this->getDbRow()->findParentModel_DbTable_Stacks()->findParentModeL_DbTable_StackGroups();
		return $stackGroupRow['id'];
	}
	
	public function processOrder() {
		if(!$this->hasEnoughStock()) throw new LogicException('Not enough stock of ' . $this->getName() . ' to fulfill order');
		$row	= $this->getDbRow();
		$row['stock_num']	-= $this->getOrderAmount();
		$row->save();
		return $this;
	}
}