<?php
class Model_Warehouse_Order_Picker {
	
	protected $_stacksGroups	= null;
	protected $_initStackGroup	= null;
	protected $_currentStackGroup	= null;
	protected $_nextStackGroup	= null;
	protected $_prevStackGroup	= null;
	protected $_lastStackGroup	= null;
	
	protected $_lastStackManaged	= 0;
	
	protected $_homeStackGroup	= null;
	
	protected $_processedStackGroups	= array();
	protected $_processedStacks	= array();
	
	protected $_route	= array();
	
	protected $_currentY	= 0;
	protected $_currentStackNum	= 1;
	
	public function __construct(Model_Warehouse_Order_StackGroup $initStack = null) {
		if($initStack != null) {
			$this->_initStackGroup	= $initStack;
			$this->_currentStackGroup	= $initStack;
			$this->processCurrentStackGroup();
		}
	}
	
	public function setStackGroups($stacks) {
		$this->_stackGroups	= $stacks;
		$this->_initStackGroup	= $stacks[0];
		$this->_nextStackGroup	= isset($stacks[1]) ? $stacks[1]:null;
		$this->_currentStackGroup	= $stacks[0];
		$this->_lastStackGroup	= end($stacks);
		reset($stacks);
		return $this;
	}
	
	public function setInitStackGroup(Model_Warehouse_Order_StackGroup $initStack) {
		$this->_initStackGroup	= $initStack;
		$this->_currentStackGroup	= $initStack;
		return $this;
	}
	
	public function getInitStackGroup() {
		return $this->_initStackGroup;
	}
	
	public function setCurrentStackGroup(Model_Warehouse_Order_StackGroup $stack) {
		$this->_currentStackGroup	= $stack;
		return $this;
	}
	
	public function setNextStackGroup(Model_Warehouse_Order_StackGroup $stack) {
		$this->_nextStackGroup	= $stack;
		return $this;
	}
	
	public function setLastStackGroup(Model_Warehouse_Order_StackGroup $stack) {
		$this->_lastStackGroup	= $stack;
		return $this;
	}
	
	public function getLastStackGroup() {
		return $this->_lastStackGroup;
	}
	
	public function getNextStackGroup() {
		return $this->_nextStackGroup;
	}
	
	public function getPrevStackGroup() {
		return $this->_prevStackGroup;
	}
	
	public function getCurrentStackGroup() {
		return $this->_currentStackGroup;
	}
	
	public function getCurrentY() {
		return $this->_currentY;
	}
	
	public function setCurrentY($int) {
		$this->_currentY	= intval($int);
		return $this;
	}
	
	public function getCurrentStackNum() {
		return $this->_currentStackNum;
	}
	
	public function setCurrentStackNum($int) {
		$this->_currentStackNum	= intval($int);
		return $this;
	}
	
	public function setHomeStackGroup(Model_Warehouse_Order_StackGroup $stack) {
		$this->_homeStackGroup	= $stack;
		return $this;
	}
	
	public function getHomeStackGroup() {
		return $this->_homeStackGroup;
	}
	
	public function processStackGroups() {
		foreach($this->_stackGroups as $stackGroup) {
			if($stackGroup == $this->getInitStackGroup()) continue;
			$this->setNextStackGroup($stackGroup);
			$this->processCurrentStackGroup();
		}
		$this->processCurrentStackGroup();
	}
	
	protected function _moveBetweenStackGroups(Model_Warehouse_Order_StackGroup $stackStart, Model_Warehouse_Order_StackGroup $stackEnd) {
		$groupsInBetween	= $this->getStackGroupDistance($stackStart, $stackEnd);
		$rightMoveUnit	= 0;
		if($this->getCurrentStackNum() == 1) {
			$rightMoveUnit	+= 3;
			if(!$stackEnd->firstStackHasProduct()) $rightMoveUnit	+= 3;
		}
		elseif(!$stackEnd->getFirstStack()->hasProducts() && !in_array($stackEnd->getFirstStack()->getId(), $this->_processedStacks) && $groupsInBetween > 0) {
			$rightMoveUnit += 3; //now moved to the right side of the next stack group;
		}
		elseif(!in_array($stackEnd->getFirstStack()->getId(), $this->_processedStacks) && $groupsInBetween < 1) {
			$rightMoveUnit += 3;
		}
		$rightMoveUnit	+= ($groupsInBetween * 3); //now moved to the left side of the next stack group
		if($rightMoveUnit > 0) $this->addRoute("Move right {$rightMoveUnit}m");
	}
	
	public function processCurrentStackGroup(Model_Warehouse_Order_StackGroup $stack = null) {
		if($stack == null) $stack	= $this->getCurrentStackGroup();
		if($stack &&!in_array($stack->getId(), $this->_processedStackGroups)) {
			if($this->getPrevStackGroup() != null) {
				$this->_moveBetweenStackGroups($this->getPrevStackGroup(), $stack);
			}
			
			$this->_processedStackGroups[]	= $stack->getId();
			$thisStackGroup	= $this->getCurrentStackGroup();
			$stack_1	= $thisStackGroup->getFirstStack();
			$stack_2	= $thisStackGroup->getSecondStack();
			if($this->_initStackGroup == $thisStackGroup) {
				$stack_1	= $thisStackGroup->getFirstStack();
				$this->setHomeStackGroup($thisStackGroup);
				if($stack_1->hasProducts()) {
					$this->addRoute('Start from ' . $thisStackGroup->getName());
					$this->addRoute('Move left 1m');
				}
				else {
					$nextGroup	= $thisStackGroup->getNextGroup();
					if($nextGroup) {
						$this->setHomeStackGroup($nextGroup);
						$this->addRoute('Start from ' . $nextGroup->getName());
						$this->addRoute('Move left 1m');
					}
					else {
						$this->addRoute('Start from' . $thisStackGroup->getName());
						$this->addRoute('Move right 2');
					}
				}
			}
			
			$prevStackNum	= $this->getCurrentStackNum();
			if($stack_1->hasProducts()) {
				$usesStack2	= $stack_2->hasProducts();
				$this->setCurrentStackNum(1);
				$result	= $this->_processStack($stack_1, $thisStackGroup);
				//if($result === null) $this->setCurrentStackNum($prevStackNum); //likely reason is that the stack has been already processed in the previous operation
				if($usesStack2) {
					$this->_determineYMovement($stack_2, $thisStackGroup->getMaxHeight());
					$this->addRoute('Move right 3m');
					$this->setCurrentStackNum(2);
					$result	= $this->_processStack($stack_2, $thisStackGroup);
					if($result === null) $this->setCurrentStackNum($prevStackNum);
					
					if($stack == $this->getLastStackGroup()) $this->returnHome();
				}
				else {
					if($stack == $this->getLastStackGroup()) $this->returnHome();
					else {
						$nextGroup	= $this->getNextStackGroup();
						$maxHeight	= $thisStackGroup->getMaxHeight() > $nextGroup->getMaxHeight() ? $thisStackGroup->getMaxHeight:$nextGroup->getMaxHeight();
						$targetStack	= $nextGroup->firstStackHasProduct() ? $nextGroup->getFirstStack():$nextGroup->getSecondStack();
						$this->_determineYMovement($targetStack, $maxHeight);
					}
					
				}
			}
			else {
				$this->setCurrentStackNum(2);
				$result	= $this->_processStack($stack_2, $thisStackGroup);
				if($result === null) $this->setCurrentStackNum($prevStackNum);
				
				$nextGroup	= $this->getNextStackGroup();
				$maxHeight	= $thisStackGroup->getMaxHeight() > $nextGroup->getMaxHeight() ? $thisStackGroup->getMaxHeight:$nextGroup->getMaxHeight();
				$targetStack	= $nextGroup->firstStackHasProduct() ? $nextGroup->getFirstStack():$nextGroup->getSecondStack();
				
				if($stack == $this->getLastStackGroup()) $this->returnHome();
				else $this->_determineYMovement($targetStack, $maxHeight);
			}
		}
		
		return $this->_shiftNextStackGroups();
	}
	
	protected function _shiftNextStackGroups() {
		$this->_prevStackGroup	= $this->getCurrentStackGroup();
		$this->_currentStackGroup	= $this->getNextStackGroup();
		return $this;
	}
	
	protected function _determineYMovement(Model_Warehouse_Order_StackGroup_Stack $nextStack, $stackGroupHeight) {
		$upperY	= $stackGroupHeight + 1;
		$midY	= $stackGroupHeight / 2;
		
		$products	= $nextStack->getProducts();
		
		$minBinNum	= $products[0]->getBinNum();
		$products->sortFromTop();
		$maxBinNum	= $products[0]->getBinNum();
		$products->sortFromBottom();
		
		$upwardSum	= ($upperY - $this->getCurrentY()) + ($upperY - $maxBinNum);
		$downwardSum	= $this->getCurrentY() + $maxBinNum;
		if($downwardSum > $upwardSum) {
			//move upwards
			$direction	= 'up';
			$destPos	= 11;
			$distance	= $destPos - $this->getCurrentY();
		}
		else {
			//move down
			$direction	= 'down';
			$destPos	= 0;
			$distance	= abs($destPos - $this->getCurrentY());
		}
		if($distance > 0) {
			$this->addRoute("Move {$direction} {$distance}m");
			$this->setCurrentY($destPos);
		}
		
		
		return $this;
	}
	
	protected function _processStack(Model_Warehouse_Order_StackGroup_Stack $stack, Model_Warehouse_Order_StackGroup $parentStackGroup) {
		if(!in_array($stack->getId(), $this->_processedStacks)) {
			$this->_lastStackManaged	= $parentStackGroup->getId();
			$this->_processedStacks[]	= $stack->getId();
			
			if($this->getCurrentY() == 0) {
				$products	= $stack->getProducts();
				$direction	= 'up';
			}
			else {
				$products	= $stack->getProducts('top');
				$direction	= 'down';
			}
			
			$combinedStacks	= false;
			$neighborStack	= $parentStackGroup->getNextGroup();
			if($neighborStack != null && $this->getNextStackGroup() != null) {
				if($this->getCurrentStackNum() == 2 && $neighborStack->getId() == $this->getNextStackGroup()->getId() /*&& $neighborStack->firstStackHasProduct()*/) {
					$neighborStack	= null;
					$neighborStack	= $this->getNextStackGroup();
					if($neighborStack->firstStackHasProduct()) {
						$combinedStacks	= true;
						$targetStack	= $neighborStack->getFirstStack();
						$this->_processedStacks[]	= $targetStack->getId();
						if($this->getCurrentY() == 0) $secondProducts	= $targetStack->getProducts();
						else $secondProducts	= $targetStack->getProducts('top');
							
						$tempProducts	= array();
						foreach($products as $product) $tempProducts[$product->getBinNum()][]	= $product;
						foreach($secondProducts as $product) $tempProducts[$product->getBinNum()][]	= $product;
					}
				}
			}
			
			if($combinedStacks) {
				foreach($tempProducts as $binNum => $products) {
					$destPos	= $binNum;
					$distance	= abs($destPos - $this->getCurrentY());
					$productSet	= array();
					foreach($products as $product) {
						$productSet[]	= $product->getBin();
						$product->processOrder();
					}
			
					$this->addRoute("Move {$direction} {$distance}m to product in " . implode(' and ', $productSet));
					$this->setCurrentY($destPos);
				}
			}
			else {
				foreach($products as $product) {
					$destPos	= $product->getBinNum();
					$distance	= abs($destPos - $this->getCurrentY());
					$this->addRoute("Move {$direction} {$distance}m to product in " . $product->getBin());
					$this->setCurrentY($destPos);
					$product->processOrder();
				}
			}
		}
		else return null;
		
		return $this;
	}
	
	public function returnHome() {
		if($this->getCurrentY() > 0) {
			$distance	= $this->getCurrentY() - 0;
			$this->addRoute("Move down {$distance}m");
			$this->setCurrentY(0);
		}
			
		if($this->getInitStackGroup() == $this->getLastStackGroup()) {
			if($this->getCurrentStackNum() == 2) {
				$distance	= 2;
				$direction	= 'left';
			}
			else {
				$distance	= 1;
				$direction	= 'right';
			}
			$this->addRoute("Move {$direction} {$distance}m to home");
		}
		else {
			if($this->getLastStackGroup()->getId() == $this->getHomeStackGroup()->getId()) {
				if($this->getCurrentStackNum() == 1) $this->addRoute("Move right 1m to home");
				else $this->addRoute("Move left 2m to home");
			}
			else {
				$groupsInBetween	= $this->getStackGroupDistance($this->getHomeStackGroup(), $this->getLastStackGroup());
				$distance	= 0;
				if($this->getCurrentStackNum() == 2) {
					if($this->getLastStackGroup()->getId() == $this->_lastStackManaged) $distance	+= 5;
					else $distance	+= 2;
				}
				else $distance	+= 2;
				
				$distance	+= ($groupsInBetween * 3);
				$this->addRoute("Move left {$distance}m to home");
			}
		}
		return $this;
	}
	
	public function getStackGroupDistance(Model_Warehouse_Order_StackGroup $group1, Model_Warehouse_Order_StackGroup $group2) {
		$thisTable	= $group1->getDbRow()->getTable();
		$select	= $thisTable	->select()
								->from('stack_groups', array('betweenCount' => 'COUNT(*)'))
								->where('`order` > ?', $group1->getOrder())
								->where('`order` < ?', $group2->getOrder());
		$result	= $thisTable->fetchRow($select);
		return $result['betweenCount'];
	}
	
	public function addRoute($message) {
		$this->_route[]	= $message;
		return $this;
	}
	
	public function getRoute() {
		return $this->_route;
	}
}