<?php
class Model_Warehouse_Order_StackGroup {
	
	protected $_dbRow	= null;
	
	protected $_id = null, $_name = null, $_order = null;
	
	protected $_stacks	= array();
	protected $_stackIds	= array();
	
	public function __construct(Zend_Db_Table_Row_Abstract $thisRow) {
		if($thisRow->getTableClass() != 'Model_DbTable_StackGroups') throw new InvalidArgumentException('Supplied row is not a stack group');
		
		$this->_dbRow	= $thisRow;
		$this->_bindRowToClass();
		$this->_buildStacks();
	}
	
	public function getDbRow() {
		return $this->_dbRow;
	}
	
	protected function _bindRowToClass() {
		$thisRow	= $this->getDbRow();
		$this->_id	= $thisRow['id'];
		$this->_name	= $thisRow['name'];
		$this->_order	= $thisRow['order'];
		return $this;
	}
	
	public function getId() {
		return $this->_id;
	}
	
	public function getName() {
		return $this->_name;
	}
	
	public function setName($name) {
		$this->_name	= htmlentities(strip_tags(strval($name)));
		return $this;
	}
	
	public function getOrder() {
		return $this->_order;
	}
	
	public function setOrder($order) {
		$this->_order	= intval($order);
		return $this;
	}
	
	public function toArray() {
		
	}
	
	protected function _buildStacks() {
		$select	= $this->getDbRow()->select()->order('order ASC');
		$stacks	= $this->getDbRow()->findModel_DbTable_Stacks($select);
		
		$firstStack	= new Model_Warehouse_Order_StackGroup_Stack($stacks[0]); 
		$this->_stacks[0]	= $firstStack;
		$this->_stackIds[$firstStack->getId()]	= 0;
		if(count($stacks) > 1) {
			$secondStack	= new Model_Warehouse_Order_StackGroup_Stack($stacks[1]);
			$this->_stacks[1]	= $secondStack;
			$this->_stackIds[$secondStack->getId()]	= 1;
		}
		return $this;
	}
	
	public function save() {
		$thisRow	= $this->getDbRow();
		$thisRow['name']	= $this->getName();
		$thisRow['order']	= $this->getOrder();
		$thisRow->save();
		return $this->_bindRowToClass();
	}
	
	public function getNextGroup() {
		$select	= $this	->getDbRow()->select()
						->from('stack_groups', array('id'))
						->where('`order` >= ?', $this->getOrder())
						->where('name > ?', $this->getName())
						->order(array('order ASC', 'name ASC'));
		$row	= $this->getDbRow()->getTable()->fetchRow($select);
		if($row) return new Model_Warehouse_Order_StackGroup($this->getDbRow()->getTable()->find($row['id'])->current());
		else return null;
	}
	
	public function addProduct(Model_Warehouse_Order_Product $thisProduct) {
		if(isset($this->_stackIds[$thisProduct->getStackId()])) {
			$targetStack	= $this->_stacks[$this->_stackIds[$thisProduct->getStackId()]];
			$targetStack->addProduct($thisProduct);
		}
	}
	
	public function firstStackHasProduct() {
		return $this->_stacks[0]->hasProducts();
	}
	
	public function secondStackHasProduct() {
		if(isset($this->_stacks[1])) return $this->_stacks[1]->hasProducts();
		return false;
	}
	
	public function getFirstStack() {
		return	$this->_stacks[0];
	}
	
	public function getSecondStack() {
		return $this->_stacks[1];
	}
	
	public function getMaxHeight() {
		$stack1Height	= $this->getFirstStack()->getMaxBins();
		$stack2Height	= $this->getSecondStack()->getMaxBins();
		
		$height	= $stack2Height > $stack1Height ? $stack2Height:$stack1Height;
		return $height;
	}
}