<?php
abstract class Model_Warehouse_Abstract implements JsonSerializable {
	abstract public function toArray();
	
	public function jsonSerialize() {
		return $this->toArray();
	}
}