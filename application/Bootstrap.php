<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initDatabase() {
        $options	= $this->getOption('database');
        $db			= Zend_Db::factory($options['adapter'], $options['params']);
        Zend_Db_Table_Abstract::setDefaultAdapter($db);
        return $db;
    }
    
    protected function _initDocType() {
    	if(APPLICATION_ENV != 'testing') {
    		$this->bootstrap('view');
    		$view	= $this->getResource('view');
    		$view->doctype('XHTML1_STRICT');
    		$view->headTitle('LP Warehouse');
    	}
	}
	
}