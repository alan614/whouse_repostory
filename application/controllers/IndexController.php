<?php

class IndexController extends Zend_Controller_Action
{
	protected $_warehouse	= null;
    public function init()
    {
        /* Initialize action controller here */
    	$this->_warehouse	= new Model_Warehouse(new Model_DbTable_Products(), new Model_DbTable_Stacks(), new Model_DbTable_StackGroups);
    }

    public function indexAction()
    {
        // action body
    }
    
    public function processOrderAction() {
    	if($this->_request->isPost()) {
    		$route	= array();
    		$products	= array();
    		$hasError	= false;
    		$data	= $this->_request->getPost();
    		$products	= json_decode($data['order'], true);
    		$order	= $this->_warehouse->createOrder();
    		foreach($products as $data) $order->addProductById($data['id'], $data['orderCount']);
    		$report	= $order->evaluateOrderAvailability();
    		if($report['available']) {
    			$route	= $order->findRoute();
    			$products	= $order->getProducts();
    		}
    		else {
    			$hasError	= true;
    			$this->view->report	= $report;
    		}
    		
    		$this->view->hasError	= $hasError;
    		$this->view->route	= $route;
    		$this->view->products	= $products;
    	}
    }

	public function generateProductsAction() {
		$dbStacks	= new Model_DbTable_Stacks();
		$dbProducts	= new Model_DbTable_Products();
		
		$stacks	= $dbStacks->fetchAll();
		$dbProducts->getAdapter()->query('TRUNCATE TABLE products');
		foreach($stacks as $stack) {
			for($i = 1; $i < 11; $i++) {
				$dbProducts->insert(array(
					'stack_id'	=> $stack['id'],
					'bin_num'	=> $i,
					'name'	=> 'product - ' . $stack['name'] . $i,
					'stock_num'	=> mt_rand(1, 5),
					'description'	=> 'Lorem ipsum product-' . $stack['name'] . $i
				));
			}
		}
		exit('products have been generated');
	}
	
	public function jsonGetProductsAction() {
		$thisPage	= $this->_getParam('page', 1);
		$thisName	= $this->_getParam('name', '');
		$thisMode	= $this->_getparam('mode', 'product-name');
		if($thisMode == 'bin' && $thisName != '') {
			preg_match("/^[a-zA-Z]+/", $thisName, $matches);
			if(!empty($matches)) {
				$stackName	= $matches[0];
				$thisStack	= $this->_warehouse->getStackByName($stackName);
				preg_match("/[0-9]+$/", $thisName, $matches);
				if(!empty($matches)) {
					$thisBinNum	= $matches[0];
					$product	= $thisStack->getProductByBinNum($thisBinNum);
					$output	= array(
						'items'	=> array($product),
						'itemPerPage'	=> $thisStack->getResultSetLimit(),
						'totalItems'	=> 1,
						'currentPage'	=> 1
					);
				}
				else  {
					$products	= $thisStack->getProducts($thisPage);
					$output	= array(
						'items'	=> $products,
						'itemPerPage'	=> $products->getItemPerPage(),
						'totalItems'	=> $products->getTotalItemCount(),
						'currentPage'	=> $products->getCurrentPage()
					);
				}
			}
			else {
				$thisName	= '';
				goto prod;
			}
			
		}
		else {
			prod:
			$products	= $this->_warehouse->getProducts($thisName, $thisPage);
			$output	= array(
			'items'	=> $products,
			'itemPerPage'	=> $products->getItemPerPage(),
			'totalItems'	=> $products->getTotalItemCount(),
			'currentPage'	=> $products->getCurrentPage()
			);
		}
		
		$this->_helper->json($output, true, array('enableJsonExprFinder' => true));
	}
	
	public function jsonGetProductInfoAction() {
		$productId	= $this->_getParam('id', 0);
		$thisProduct	= $this->_warehouse->getProductById($productId);
		if(!$thisProduct) $thisProduct	= array();
		$this->_helper->json($thisProduct, true, array('enableJsonExprFinder' => true));
	}
}

