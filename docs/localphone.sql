-- MySQL dump 10.13  Distrib 5.5.44, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: localphone
-- ------------------------------------------------------
-- Server version	5.5.44-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stack_id` int(11) NOT NULL,
  `bin_num` int(11) NOT NULL DEFAULT '0',
  `name` varchar(256) NOT NULL,
  `description` text,
  `stock_num` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_bin` (`stack_id`,`bin_num`),
  KEY `stack_id` (`stack_id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,1,1,'product - A1','Lorem ipsum product-A1',5),(2,1,2,'product - A2','Lorem ipsum product-A2',2),(3,1,3,'product - A3','Lorem ipsum product-A3',5),(4,1,4,'product - A4','Lorem ipsum product-A4',3),(5,1,5,'product - A5','Lorem ipsum product-A5',1),(6,1,6,'product - A6','Lorem ipsum product-A6',5),(7,1,7,'product - A7','Lorem ipsum product-A7',4),(8,1,8,'product - A8','Lorem ipsum product-A8',3),(9,1,9,'product - A9','Lorem ipsum product-A9',5),(10,1,10,'product - A10','Lorem ipsum product-A10',5),(11,2,1,'product - B1','Lorem ipsum product-B1',2),(12,2,2,'product - B2','Lorem ipsum product-B2',3),(13,2,3,'product - B3','Lorem ipsum product-B3',4),(14,2,4,'product - B4','Lorem ipsum product-B4',2),(15,2,5,'product - B5','Lorem ipsum product-B5',2),(16,2,6,'product - B6','Lorem ipsum product-B6',1),(17,2,7,'product - B7','Lorem ipsum product-B7',4),(18,2,8,'product - B8','Lorem ipsum product-B8',5),(19,2,9,'product - B9','Lorem ipsum product-B9',4),(20,2,10,'product - B10','Lorem ipsum product-B10',2),(21,3,1,'product - C1','Lorem ipsum product-C1',3),(22,3,2,'product - C2','Lorem ipsum product-C2',4),(23,3,3,'product - C3','Lorem ipsum product-C3',3),(24,3,4,'product - C4','Lorem ipsum product-C4',1),(25,3,5,'product - C5','Lorem ipsum product-C5',2),(26,3,6,'product - C6','Lorem ipsum product-C6',3),(27,3,7,'product - C7','Lorem ipsum product-C7',5),(28,3,8,'product - C8','Lorem ipsum product-C8',4),(29,3,9,'product - C9','Lorem ipsum product-C9',5),(30,3,10,'product - C10','Lorem ipsum product-C10',3),(31,4,1,'product - D1','Lorem ipsum product-D1',3),(32,4,2,'product - D2','Lorem ipsum product-D2',1),(33,4,3,'product - D3','Lorem ipsum product-D3',4),(34,4,4,'product - D4','Lorem ipsum product-D4',4),(35,4,5,'product - D5','Lorem ipsum product-D5',4),(36,4,6,'product - D6','Lorem ipsum product-D6',3),(37,4,7,'product - D7','Lorem ipsum product-D7',3),(38,4,8,'product - D8','Lorem ipsum product-D8',4),(39,4,9,'product - D9','Lorem ipsum product-D9',2),(40,4,10,'product - D10','Lorem ipsum product-D10',1),(41,5,1,'product - E1','Lorem ipsum product-E1',2),(42,5,2,'product - E2','Lorem ipsum product-E2',3),(43,5,3,'product - E3','Lorem ipsum product-E3',3),(44,5,4,'product - E4','Lorem ipsum product-E4',1),(45,5,5,'product - E5','Lorem ipsum product-E5',2),(46,5,6,'product - E6','Lorem ipsum product-E6',2),(47,5,7,'product - E7','Lorem ipsum product-E7',1),(48,5,8,'product - E8','Lorem ipsum product-E8',1),(49,5,9,'product - E9','Lorem ipsum product-E9',2),(50,5,10,'product - E10','Lorem ipsum product-E10',1),(51,6,1,'product - F1','Lorem ipsum product-F1',5),(52,6,2,'product - F2','Lorem ipsum product-F2',2),(53,6,3,'product - F3','Lorem ipsum product-F3',5),(54,6,4,'product - F4','Lorem ipsum product-F4',3),(55,6,5,'product - F5','Lorem ipsum product-F5',3),(56,6,6,'product - F6','Lorem ipsum product-F6',4),(57,6,7,'product - F7','Lorem ipsum product-F7',5),(58,6,8,'product - F8','Lorem ipsum product-F8',5),(59,6,9,'product - F9','Lorem ipsum product-F9',2),(60,6,10,'product - F10','Lorem ipsum product-F10',3);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stack_groups`
--

DROP TABLE IF EXISTS `stack_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stack_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order` (`order`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stack_groups`
--

LOCK TABLES `stack_groups` WRITE;
/*!40000 ALTER TABLE `stack_groups` DISABLE KEYS */;
INSERT INTO `stack_groups` VALUES (1,'P1',1),(2,'P2',2),(3,'P3',3);
/*!40000 ALTER TABLE `stack_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stacks`
--

DROP TABLE IF EXISTS `stacks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stacks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stack_group_id` int(11) NOT NULL,
  `name` varchar(8) NOT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `stack_group_id` (`stack_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stacks`
--

LOCK TABLES `stacks` WRITE;
/*!40000 ALTER TABLE `stacks` DISABLE KEYS */;
INSERT INTO `stacks` VALUES (1,1,'A',1),(2,1,'B',2),(3,2,'C',1),(4,2,'D',2),(5,3,'E',1),(6,3,'F',2);
/*!40000 ALTER TABLE `stacks` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-19 16:08:30
